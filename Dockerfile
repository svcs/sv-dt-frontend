FROM node:12-stretch AS build

ARG DEFAULT_API_ROOT
ENV API_ROOT=${DEFAULT_API_ROOT:-"http://0.0.0.0:8080"}
WORKDIR /home/node/
COPY . .
RUN sed -i "s#https://conduit.productionready.io/api#${API_ROOT}#" ./src/agent.js \
    && cat ./src/agent.js \
    && npm install \
    && npm run build

FROM nginx

ENV APP_DIR="/usr/share/nginx/html/" \
    BUILD_DIR="/home/node/build/"
    
COPY --from=build --chown=nginx:nginx ${BUILD_DIR} ${APP_DIR}
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
